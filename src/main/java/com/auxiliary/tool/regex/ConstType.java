package com.auxiliary.tool.regex;

/**
 * <p>
 * <b>文件名：ConstType.java</b>
 * </p>
 * <p>
 * <b>用途：</b> 定义各个常用参数，以便于统一管理
 * </p>
 * <p>
 * <b>编码时间：2022年3月30日 上午8:32:30
 * </p>
 * <p>
 * <b>修改时间：2022年3月30日 上午8:32:30
 * </p>
 *
 *
 * @author 彭宇琦
 * @version Ver1.0
 * @since JDK 1.8
 * @since autest 3.3.0
 */
public class ConstType {
    /**
     * 定义map集合默认的大小
     *
     * @since autest 3.3.0
     */
    public static final int DEFAULT_MAP_SIZE = 16;
}
